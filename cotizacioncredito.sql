-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-12-2021 a las 18:18:29
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cotizacioncredito`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plazo`
--

CREATE TABLE `plazo` (
  `ID_PLAZO` int(11) NOT NULL,
  `NUMERO_SEMANAS` int(11) NOT NULL,
  `TAZA_NORMAL` float NOT NULL,
  `TAZA_PUNTUAL` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `plazo`
--

INSERT INTO `plazo` (`ID_PLAZO`, `NUMERO_SEMANAS`, `TAZA_NORMAL`, `TAZA_PUNTUAL`) VALUES
(1, 12, 7.56, 2.32),
(4, 24, 43, 21.55),
(5, 36, 12.45, 5.55),
(9, 48, 12, 22);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `ID_PRODUCTO` int(10) NOT NULL,
  `SKU` varchar(12) NOT NULL,
  `NOMBRE` varchar(50) NOT NULL,
  `PRECIO` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`ID_PRODUCTO`, `SKU`, `NOMBRE`, `PRECIO`) VALUES
(22, '23LKJLK33312', 'TV 55', 13500),
(23, 'KJJJ3380U23E', 'Lavadora', 8454.4),
(24, 'DLKJJ3339DDD', 'Colchon Air', 19700);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `plazo`
--
ALTER TABLE `plazo`
  ADD PRIMARY KEY (`ID_PLAZO`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`ID_PRODUCTO`),
  ADD UNIQUE KEY `SKU` (`SKU`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `plazo`
--
ALTER TABLE `plazo`
  MODIFY `ID_PLAZO` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `ID_PRODUCTO` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package com.BancoAzteca.CotizadorCreditos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CotizadorCreditosApplication {

	public static void main(String[] args) {
		SpringApplication.run(CotizadorCreditosApplication.class, args);
	}

}

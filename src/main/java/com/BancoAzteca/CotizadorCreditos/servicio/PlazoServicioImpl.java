package com.BancoAzteca.CotizadorCreditos.servicio;

import com.BancoAzteca.CotizadorCreditos.entidad.Plazo;
import com.BancoAzteca.CotizadorCreditos.repositorio.PlazoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class PlazoServicioImpl implements  PlazoServicio{

    @Autowired
    private PlazoRepositorio plazoRepositorio;

    @Override
    @Transactional(readOnly=true)
    public Iterable<Plazo> findAll() {
        return plazoRepositorio.findAll();
    }

    @Override
    public Optional<Plazo> findByNumeroSemandas(int numero) {
        return plazoRepositorio.findPlazoByNumeroSemanas(numero);
    }

    @Override
    public Optional<Plazo> findById(Long numero) {
        return plazoRepositorio.findById(numero);
    }

    @Override
    public void deleteById(Long id) {
        plazoRepositorio.deleteById(id);
    }

    @Override
    public Boolean existPlazoById(Long id) {
        return plazoRepositorio.existsById(id);
    }

    @Override
    public Plazo savePlazo(Plazo plazo) {
        return plazoRepositorio.save(plazo);
    }

    @Override
    public void updatePlazo(Plazo plazo) {
        plazoRepositorio.update(plazo.getNumeroSemanas(),plazo.getTazaNormal(),plazo.getTazaPuntual(),plazo.getID_PLAZO());

    }
}

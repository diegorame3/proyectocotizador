package com.BancoAzteca.CotizadorCreditos.servicio;

import com.BancoAzteca.CotizadorCreditos.entidad.Pago;
import com.BancoAzteca.CotizadorCreditos.entidad.Plazo;
import com.BancoAzteca.CotizadorCreditos.entidad.Producto;

import java.util.ArrayList;

public interface CotizadorServicio {

    Pago calcularPagos(Iterable<Plazo> plazos, Producto producto,int numeroSemanas);
}

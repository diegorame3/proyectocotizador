package com.BancoAzteca.CotizadorCreditos.servicio;

import com.BancoAzteca.CotizadorCreditos.entidad.Producto;

import java.util.Optional;

public interface ProductoServicio {
    Iterable<Producto> findAll();
    Optional<Producto> findById(Long id);
    //buscar por nombre
    Optional<Producto> findByName(String name);
    //buscar por SKU
    Optional<Producto> findBySKU(String sku);
    void deleteById(Long id);
    Boolean existProductoById(Long id);
    Producto saveProducto(Producto producto);
    void updateProducto(Producto producto);

}

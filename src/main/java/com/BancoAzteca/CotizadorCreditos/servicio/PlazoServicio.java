package com.BancoAzteca.CotizadorCreditos.servicio;

import com.BancoAzteca.CotizadorCreditos.entidad.Plazo;
import com.BancoAzteca.CotizadorCreditos.entidad.Producto;

import java.util.Optional;

public interface PlazoServicio {
    Iterable<Plazo> findAll();
    Optional<Plazo> findByNumeroSemandas(int numero);
    Optional<Plazo> findById(Long numero);
    void deleteById(Long id);
    Boolean existPlazoById(Long id);
    Plazo savePlazo(Plazo plazo);
    void updatePlazo(Plazo plazo);
}

package com.BancoAzteca.CotizadorCreditos.servicio;

import com.BancoAzteca.CotizadorCreditos.entidad.Pago;
import com.BancoAzteca.CotizadorCreditos.entidad.Plazo;
import com.BancoAzteca.CotizadorCreditos.entidad.Producto;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CotizadorServicioImpl implements CotizadorServicio{

    @Override
    public Pago calcularPagos(Iterable<Plazo> plazos, Producto producto,int numeroSemanas) {
        Pago pagos =null;

        if(numeroSemanas!=0){
            for (Plazo plazo:plazos) {
                if (plazo.getNumeroSemanas()==numeroSemanas){
                    pagos = new Pago(numeroSemanas,((producto.getPrecio()*plazo.getTazaNormal())+producto.getPrecio())/plazo.getNumeroSemanas(),(producto.getPrecio()*plazo.getTazaPuntual()+producto.getPrecio())/plazo.getNumeroSemanas());
                }
            }
        }else{
           pagos=new Pago(0);
        }
        return pagos;
    }
}

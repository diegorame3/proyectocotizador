package com.BancoAzteca.CotizadorCreditos.servicio;

import com.BancoAzteca.CotizadorCreditos.entidad.Producto;
import com.BancoAzteca.CotizadorCreditos.repositorio.ProductoRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class ProductoServicioImpl implements ProductoServicio {

    @Autowired
    private ProductoRepositorio productoRepositorio;


    @Override
    @Transactional(readOnly=true)
    public Iterable<Producto> findAll() {
        return productoRepositorio.findAll();
    }

    @Override
    public Optional<Producto> findById(Long id) {
        return productoRepositorio.findById(id);
    }


    @Override
    public Optional<Producto> findByName(String name) {
        return productoRepositorio.findProductoByNombre(name);
    }

    @Override
    public Optional<Producto> findBySKU(String sku) {
        return productoRepositorio.findProductoBySku(sku);
    }

    @Override
    public void deleteById(Long id) {
        productoRepositorio.deleteById(id);
    }

    @Override
    public Boolean existProductoById(Long id) {
        return productoRepositorio.existsById(id);
    }

    @Override
    public Producto saveProducto(Producto producto) {
        return  productoRepositorio.save(producto);
    }

    @Override
    public void updateProducto(Producto producto) {
        productoRepositorio.update(producto.getNombre(),producto.getSku(),producto.getPrecio(),producto.getID_PRODUCTO());
    }


}

package com.BancoAzteca.CotizadorCreditos.entidad;

public class Pago {

    int numSemanas;
    Float abonoNormal;
    Float abonoPuntual;

    public Pago() {

    }

    public Pago(int numSemanas) {
        float inicio=0;
        this.numSemanas = numSemanas;
        this.abonoNormal=inicio;
        this.abonoPuntual=inicio;

    }

    public Pago(int numSemanas, Float abonoNormal, Float abonoPuntual) {
        this.numSemanas=numSemanas;
        this.abonoNormal = abonoNormal;
        this.abonoPuntual = abonoPuntual;
    }

    public int getNumSemanas() {
        return numSemanas;
    }

    public void setNumSemanas(int numSemanas) {
        this.numSemanas = numSemanas;
    }

    public Float getAbonoNormal() {
        return abonoNormal;
    }

    public void setAbonoNormal(Float abonoNormal) {
        this.abonoNormal = abonoNormal;
    }

    public Float getAbonoPuntual() {
        return abonoPuntual;
    }

    public void setAbonoPuntual(Float abonoPuntual) {
        this.abonoPuntual = abonoPuntual;
    }
}

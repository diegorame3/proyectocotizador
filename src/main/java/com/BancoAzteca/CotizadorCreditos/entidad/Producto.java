package com.BancoAzteca.CotizadorCreditos.entidad;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="producto")
public class Producto implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long ID_PRODUCTO  ;

    @Column(name= "SKU", nullable=false, length=12, unique=true)
    private String  sku;

    @Column(name= "NOMBRE")
    private String  nombre;

    @Column(name= "PRECIO")
    private float  precio;

    public Producto() {
    }

    public Producto(String sku, String nombre, float precio) {
        this.sku = sku;
        this.nombre = nombre;
        this.precio = precio;
    }

    public Long getID_PRODUCTO() {
        return ID_PRODUCTO;
    }

    public void setID_PRODUCTO(Long ID_PRODUCTO) {
        this.ID_PRODUCTO = ID_PRODUCTO;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
}

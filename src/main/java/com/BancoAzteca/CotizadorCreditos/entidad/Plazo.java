package com.BancoAzteca.CotizadorCreditos.entidad;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="plazo")
public class Plazo implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long ID_PLAZO;

    @Column(name= "NUMERO_SEMANAS")
    private int  numeroSemanas;

    @Column(name= "TAZA_NORMAL")
    private float  tazaNormal;

    @Column(name= "TAZA_PUNTUAL")
    private float tazaPuntual;

    public Plazo() {
    }

    public Plazo(int numeroSemanas, float tazaNormal, float tazaPuntual) {
        this.numeroSemanas = numeroSemanas;
        this.tazaNormal = tazaNormal;
        this.tazaPuntual = tazaPuntual;
    }

    public Long getID_PLAZO() {
        return ID_PLAZO;
    }

    public void setID_PLAZO(Long ID_PLAZO) {
        this.ID_PLAZO = ID_PLAZO;
    }

    public int getNumeroSemanas() {
        return numeroSemanas;
    }

    public void setNumeroSemanas(int numeroSemanas) {
        this.numeroSemanas = numeroSemanas;
    }

    public float getTazaNormal() {
        return tazaNormal;
    }

    public void setTazaNormal(int tazaNormal) {
        this.tazaNormal = tazaNormal;
    }

    public float getTazaPuntual() {
        return tazaPuntual;
    }

    public void setTazaPuntual(int tazaPuntual) {
        this.tazaPuntual = tazaPuntual;
    }
}

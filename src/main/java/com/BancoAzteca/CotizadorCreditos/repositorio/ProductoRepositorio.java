package com.BancoAzteca.CotizadorCreditos.repositorio;

import com.BancoAzteca.CotizadorCreditos.entidad.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface ProductoRepositorio extends JpaRepository<Producto,Long> {

    Optional<Producto> findProductoByNombre(String nombre);
    Optional<Producto> findProductoBySku(String sku);
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE `producto` SET `NOMBRE` = ?1,`SKU` =?2,`PRECIO` = ?3 WHERE `producto`.`ID_PRODUCTO` = ?4", nativeQuery = true)
    void update(String nombre,String sku,float precio,long id);

}

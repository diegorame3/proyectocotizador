package com.BancoAzteca.CotizadorCreditos.repositorio;

import com.BancoAzteca.CotizadorCreditos.entidad.Plazo;
import com.BancoAzteca.CotizadorCreditos.entidad.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface PlazoRepositorio extends JpaRepository<Plazo,Long> {

    Optional<Plazo> findPlazoByNumeroSemanas(int numero);
    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE `plazo` SET `NUMERO_SEMANAS` = ?1, `TAZA_NORMAL` = ?2, `TAZA_PUNTUAL` = ?3 WHERE `plazo`.`ID_PLAZO` = ?4", nativeQuery = true)
    void update(int numero,float  normal,float  puntual,long id);


}

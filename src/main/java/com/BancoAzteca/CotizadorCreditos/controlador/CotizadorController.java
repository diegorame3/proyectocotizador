package com.BancoAzteca.CotizadorCreditos.controlador;

import com.BancoAzteca.CotizadorCreditos.entidad.Pago;
import com.BancoAzteca.CotizadorCreditos.entidad.Plazo;
import com.BancoAzteca.CotizadorCreditos.entidad.Producto;
import com.BancoAzteca.CotizadorCreditos.servicio.CotizadorServicio;
import com.BancoAzteca.CotizadorCreditos.servicio.PlazoServicio;
import com.BancoAzteca.CotizadorCreditos.servicio.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.util.Optional;

//Notación para indicar que es un controlador
@Controller
public class CotizadorController {

    @Autowired
    ProductoServicio productoServicio;

    @Autowired
    PlazoServicio plazoServicio;

    @Autowired
    CotizadorServicio cotizadorServicio;

    @RequestMapping(value="/verCotizador",method = { RequestMethod.POST, RequestMethod.GET })
    @GetMapping("/verCotizador")
    public String verCotizador(@ModelAttribute(value = "sku")  String sku, @ModelAttribute(value = "numeroSemanas")  String numeroSemanas, ModelMap model,RedirectAttributes redirectAttrs){

        model.addAttribute("sku",sku);
        Optional<Producto> producto=productoServicio.findBySKU(sku);
        Iterable<Plazo> listaPlazo=plazoServicio.findAll();
        Pago pago= null;
        int numero=0;

        if(!numeroSemanas.equals("")){
            numero=Integer.valueOf(numeroSemanas);
        }

        if (producto.isPresent()){
            System.out.println("articulo encontrado");
            model.addAttribute("producto", producto.get());
            model.addAttribute("listaPlazo", listaPlazo);
            pago=cotizadorServicio.calcularPagos(listaPlazo,producto.get(),numero);
            model.addAttribute("listaPagos", pago);
            redirectAttrs
                    .addFlashAttribute("status", "success")
                    .addFlashAttribute("mensaje", "Producto Encontrado");
        }else{
            System.out.println("articulo no encontrado");
            model.addAttribute("producto", new Producto("--","--",0));
            model.addAttribute("listaPlazo", listaPlazo);
            model.addAttribute("listaPagos", pago);
            redirectAttrs
                    .addFlashAttribute("status", "danger")
                    .addFlashAttribute("mensaje", "Producto no encontrado");
        }
        return"paginas/CotizacionCredito/verCotizacion";
    }


}

package com.BancoAzteca.CotizadorCreditos.controlador;

import com.BancoAzteca.CotizadorCreditos.dto.ProductoDto;
import com.BancoAzteca.CotizadorCreditos.entidad.Producto;
import com.BancoAzteca.CotizadorCreditos.servicio.ProductoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Optional;

//Notación para indicar que es un controlador
@Controller
public class ProductoControlador {

    //Inyección de dependencias
    @Autowired
    ProductoServicio productoServicio;

    @GetMapping("/")
    public String index(){
        Iterable<Producto> productos = productoServicio.findAll();
        return "redirect:/verProductos";
    }

    @GetMapping("/verProductos")
    public String verProductos(ModelMap model, RedirectAttributes redirectAttrs){
        Iterable<Producto> listaProductos=null;

        try{
            listaProductos= productoServicio.findAll();
            model.addAttribute("listaProductos", listaProductos);
        }catch (Exception e){
            System.out.println("Error al listar usuarios");
            redirectAttrs
                    .addFlashAttribute("status", "danger")
                    .addFlashAttribute("mensaje", "Error al lista productos");

            model.addAttribute("listaProductos", listaProductos);
            return "paginas/Productos/verProductos";
        }
        return "paginas/Productos/verProductos";
    }

    @PostMapping("/eliminarProductos")
    public String eliminarProductos(@ModelAttribute(value = "id")  String id,RedirectAttributes redirectAttrs){
        System.out.println("el id en el controler es: "+id);
        try{
            productoServicio.deleteById(Long.parseLong(id));
            redirectAttrs
                    .addFlashAttribute("status", "success")
                    .addFlashAttribute("mensaje", "Producto eliminado correctamente");

        }catch (Exception e){
            System.out.println("Error al eliminar");
            redirectAttrs
                    .addFlashAttribute("status", "danger")
                    .addFlashAttribute("mensaje", "Error al eliminar");

            return "redirect:/verProductos";
        }
        return "redirect:/verProductos";
    }

    @PostMapping("/crearProducto")
    public String crearProducto(@Valid @ModelAttribute ProductoDto productoDto,RedirectAttributes redirectAttrs){
        System.out.println("Datos de entrada: "+productoDto);
            Producto producto= new Producto(productoDto.getSku(),productoDto.getNombre(),productoDto.getPrecio());
            try{
                productoServicio.saveProducto(producto);
                redirectAttrs
                        .addFlashAttribute("status", "success")
                        .addFlashAttribute("mensaje", "Producto creado correctamente");

            }catch (Exception e){
                System.out.println("Datos duplicados al crear:"+e.getMessage());
                redirectAttrs
                        .addFlashAttribute("status", "danger")
                        .addFlashAttribute("mensaje", "Error al crear producto posible SKU duplicado");
                return "redirect:/verProductos";
            }

        return "redirect:/verProductos";
    }

    @PostMapping("/actualizarProducto")
    public String actualizarProducto(@Valid @ModelAttribute ProductoDto productoDto,@ModelAttribute(value = "id") String id,RedirectAttributes redirectAttrs){

        Optional<Producto> producto1 =productoServicio.findById(Long.valueOf(id));
        Producto producto= new Producto(productoDto.getSku(),productoDto.getNombre(),productoDto.getPrecio());
        producto.setID_PRODUCTO(producto1.get().getID_PRODUCTO());

        try {
            productoServicio.updateProducto(producto);
            redirectAttrs
                    .addFlashAttribute("status", "success")
                    .addFlashAttribute("mensaje", "Producto actualizado correctamente");
        }catch (Exception e){
            System.out.println("datos duplicados al actualizar: "+e.getMessage());
            redirectAttrs
                    .addFlashAttribute("status", "danger")
                    .addFlashAttribute("mensaje", "Error al actualizar producto posible SKU duplicado");

            return "redirect:/verProductos";
        }
        return "redirect:/verProductos";
    }


}

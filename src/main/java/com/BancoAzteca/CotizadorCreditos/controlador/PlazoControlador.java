package com.BancoAzteca.CotizadorCreditos.controlador;

import com.BancoAzteca.CotizadorCreditos.dto.PlazoDto;
import com.BancoAzteca.CotizadorCreditos.entidad.Plazo;
import com.BancoAzteca.CotizadorCreditos.servicio.PlazoServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.validation.Valid;
import java.util.Optional;

//Notación para indicar que es un controlador
@Controller
public class PlazoControlador {

    //Inyección de dependencias
    @Autowired
    PlazoServicio plazoServicio;

    @GetMapping("/verPlazo")
    public String verPlazo(ModelMap model, RedirectAttributes redirectAttrs){
        Iterable<Plazo> listaPlazo=null;
        try{
            listaPlazo= plazoServicio.findAll();
            model.addAttribute("listaPlazo", listaPlazo);
        }catch (Exception e){
            System.out.println("Error al listar plazos");
            redirectAttrs
                    .addFlashAttribute("status", "danger")
                    .addFlashAttribute("mensaje", "Error al lista plazo");
            model.addAttribute("listaPlazo", listaPlazo);
            return "paginas/Plazo/verPlazo";
        }
        return "paginas/Plazo/verPlazo";
    }

    @PostMapping("/eliminarPlazo")
    public String eliminarPlazo(@ModelAttribute(value = "id")  String id,RedirectAttributes redirectAttrs){
        System.out.println("el id en el controler es: "+id);
        try{
            plazoServicio.deleteById(Long.parseLong(id));
            redirectAttrs
                    .addFlashAttribute("status", "success")
                    .addFlashAttribute("mensaje", "Plazo eliminado correctamente");
        }catch (Exception e){
            System.out.println("Error al eliminar");
            redirectAttrs
                    .addFlashAttribute("status", "danger")
                    .addFlashAttribute("mensaje", "Error al eliminar plazo");
            return "redirect:/verPlazo";
        }
        return "redirect:/verPlazo";
    }

    @PostMapping("/crearPlazo")
    public String crearPlazo(@Valid @ModelAttribute PlazoDto plazoDto,RedirectAttributes redirectAttrs){
        Plazo plazo= new Plazo(plazoDto.getNumeroSemanas(),plazoDto.getTazaNormal(),plazoDto.getTazaPuntual());
        try{
            Optional<Plazo> plazoPorSemana=plazoServicio.findByNumeroSemandas(plazo.getNumeroSemanas());
            if (!plazoPorSemana.isPresent()){
                plazoServicio.savePlazo(plazo);
                redirectAttrs
                        .addFlashAttribute("status", "success")
                        .addFlashAttribute("mensaje", "Plazo creado correctamente");
            }else{
                System.out.println("Error al crear el plazo , ya existe el numero de semanas");
                redirectAttrs
                        .addFlashAttribute("status", "danger")
                        .addFlashAttribute("mensaje", "Error al crear Plazo posibles semanas duplicadas");
            }
        }catch (Exception e){
            System.out.println("Error al crear:"+e.getMessage());
            redirectAttrs
                    .addFlashAttribute("status", "danger")
                    .addFlashAttribute("mensaje", "Error al crear plazo");

            return "redirect:/verPlazo";
        }

        return "redirect:/verPlazo";
    }

    @PostMapping("/actualizarPlazo")
    public String actualizarProducto(@Valid @ModelAttribute PlazoDto plazoDto,@ModelAttribute(value = "id") String id,RedirectAttributes redirectAttrs){
        //buscar que no exista el numero de semanas repetido
        Optional<Plazo> plazoPorNumeroSemanas=plazoServicio.findByNumeroSemandas(plazoDto.getNumeroSemanas());
        try {
            if (!plazoPorNumeroSemanas.isPresent() ){
                Plazo plazo= new Plazo(plazoDto.getNumeroSemanas(),plazoDto.getTazaNormal(),plazoDto.getTazaPuntual());
                plazo.setID_PLAZO(Long.valueOf(id));
                plazoServicio.updatePlazo(plazo);
                redirectAttrs
                        .addFlashAttribute("status", "success")
                        .addFlashAttribute("mensaje", "Plazo actualizado correctamente");
            }else{
                if(plazoPorNumeroSemanas.get().getNumeroSemanas()!=plazoDto.getNumeroSemanas()){
                    System.out.println("Error  al actualizar ya existen las semanas: ");
                    redirectAttrs
                            .addFlashAttribute("status", "danger")
                            .addFlashAttribute("mensaje", "Error al actualizar posibles semanas duplicadas");
                    return "redirect:/verPlazo";
                }else{
                    Plazo plazo= new Plazo(plazoDto.getNumeroSemanas(),plazoDto.getTazaNormal(),plazoDto.getTazaPuntual());
                    plazo.setID_PLAZO(Long.valueOf(id));
                    plazoServicio.updatePlazo(plazo);
                    redirectAttrs
                            .addFlashAttribute("status", "success")
                            .addFlashAttribute("mensaje", "Plazo actualizado correctamente");
                }
            }
        }catch (Exception e){
            System.out.println("Error  al actualizar: "+e.getMessage());
            redirectAttrs
                    .addFlashAttribute("status", "danger")
                    .addFlashAttribute("mensaje", "Error al actualizar");
            return "redirect:/verPlazo";
        }
        return "redirect:/verPlazo";
    }



}

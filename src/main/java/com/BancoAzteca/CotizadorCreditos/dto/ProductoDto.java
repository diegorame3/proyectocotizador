package com.BancoAzteca.CotizadorCreditos.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ProductoDto {

    @NotBlank(message = "EL SKU es obligatorio")
    private String  sku;
    @NotBlank(message = "EL nombre es obligatorio")
    private String  nombre;
    @NotNull(message = "error no puede estar vacio")
    private float  precio;

    public ProductoDto(String sku, String nombre, float precio) {
        this.sku = sku;
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
}

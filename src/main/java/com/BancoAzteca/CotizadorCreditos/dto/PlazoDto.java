package com.BancoAzteca.CotizadorCreditos.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PlazoDto {

    @NotNull(message = "EL numero de semanas en obligatorio")
    private int  numeroSemanas;
    @NotNull(message = "La tazaNormal es obligatoria")
    private float  tazaNormal;
    @NotNull(message = "La tazaPuntual es obligatoria")
    private float tazaPuntual;

    public PlazoDto() {}

    public PlazoDto(int numeroSemanas, float tazaNormal, float tazaPuntual) {
        this.numeroSemanas = numeroSemanas;
        this.tazaNormal = tazaNormal;
        this.tazaPuntual = tazaPuntual;
    }

    public int getNumeroSemanas() {
        return numeroSemanas;
    }

    public void setNumeroSemanas(int numeroSemanas) {
        this.numeroSemanas = numeroSemanas;
    }

    public float getTazaNormal() {
        return tazaNormal;
    }

    public void setTazaNormal(float tazaNormal) {
        this.tazaNormal = tazaNormal;
    }

    public float getTazaPuntual() {
        return tazaPuntual;
    }

    public void setTazaPuntual(float tazaPuntual) {
        this.tazaPuntual = tazaPuntual;
    }
}
